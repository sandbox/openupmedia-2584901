<?php
/**
 * @file
 * Improves menu positioning on node forms.
 */

/**
 * Implements hook_theme().
 */
function better_node_menu_theme() {
  return array(
    'better_node_menu_form' => array(
      'render element' => 'form',
    ),
    'better_node_menu_menu' => array(
      'variables' => array(
        'form' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_node_insert().
 */
function better_node_menu_node_insert($node) {
  if (isset($node->bettermenu)) {
    better_node_menu_save($node);
  }
}

/**
 * Implements hook_node_update().
 */
function better_node_menu_node_update($node) {
  if (isset($node->bettermenu)) {
    better_node_menu_save($node);
  }
}

/**
 * Save the menu from the node.
 *
 * @param object $node
 *   The node that is being created or updated.
 */
function better_node_menu_save($node) {
  // Update the menu.
  if (!empty($node->updated_items)) {
    // We need an mlid, so first save the new link.
    foreach ($node->updated_items as $item) {
      if ($item['mlid'] == 'newlink') {
        $item['customized'] = 1;
        $item['hidden'] = 0;
        if (isset($node->language)) {
          $item['language'] = $node->language;
        }
        $item['link_path'] = 'node/' . $node->nid;
        $newlink_mlid = menu_link_save($item);
        break;
      }
    }
    // Save remaining menu items.
    foreach ($node->updated_items as $item) {
      if ($item['mlid'] != 'newlink') {
        if ($item['plid'] == 'newlink' && !empty($newlink_mlid)) {
          $item['plid'] = $newlink_mlid;
        }
        menu_link_save($item);
      }
    }
  }

  // Delete the current link.
  if (empty($node->bettermenu['menu_name']) && !empty($node->bettermenu['mlid'])) {
    menu_link_delete($node->bettermenu['mlid']);
  }
}

/**
 * Implements hook_node_prepare().
 */
function better_node_menu_node_prepare($node) {
  if (isset($node->menu)) {
    if ($node->menu['mlid'] > 0) {
      $node->bettermenu = array(
        'menu_name'     => $node->menu['menu_name'],
        'link_title'    => $node->menu['link_title'],
        'mlid'          => $node->menu['mlid'],
        'plid'          => $node->menu['plid'],
        'weight'        => $node->menu['weight'],
        'has_children'  => $node->menu['has_children'],
      );
    }
    else {
      $node->bettermenu = array(
        'menu_name'     => 0,
        'link_title'    => '',
        'mlid'          => NULL,
        'plid'          => 0,
        'weight'        => 0,
        'has_children'  => 0,
      );
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function better_node_menu_form_node_form_alter(&$form, $form_state, $form_id) {
  // Get the language code.
  $langcode = better_node_menu_get_node_form_language($form, $form_state);

  // Get the menu names.
  $options = better_node_menu_get_menus($form['#node']->type, $langcode);

  // Add the functionality if there are any menus available.
  if (!empty($options)) {
    // Unset the current menu form.
    unset($form['menu']);

    // Add the empty option.
    array_unshift($options, t('<None>'));

    // Add an AHAH call to the language dropdown.
    if (module_exists('i18n_menu') && isset($form['language'])) {
      $form['language']['#ajax'] = array(
        'event'     => 'change',
        'callback'  => 'better_node_menu_language_callback',
        'method'    => 'replace',
        'effect'    => 'fade',
        'wrapper'   => 'bettermenu-wrapper',
      );
    }

    // Create the new form.
    $form['bettermenu'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Menu settings'),
      '#access'       => better_node_menu_form_access(),
      '#collapsible'  => TRUE,
      '#group'        => 'additional_settings',
      '#tree'         => TRUE,
    );

    // Get the menu name.
    $menuname = 0;
    if (isset($form_state['values']['bettermenu']['menu_name'])) {
      $menuname = $form_state['values']['bettermenu']['menu_name'];
    }
    elseif (!empty($form['#node']->bettermenu['menu_name'])) {
      $menuname = $form['#node']->bettermenu['menu_name'];
    }
    if (!in_array($menuname, array_keys($options), TRUE)) {
      $menuname = 0;
    }

    $form['bettermenu']['menu_name'] = array(
      '#type'          => 'select',
      '#title'         => t('Menu'),
      '#description'   => t('Please select a menu in order to add a menu item'),
      '#options'       => $options,
      '#default_value' => $menuname,
      '#ajax' => array(
        'event'     => 'change',
        'callback'  => 'better_node_menu_menuname_callback',
        'method'    => 'replace',
        'effect'    => 'fade',
        'wrapper'   => 'bettermenu-position',
      ),
    );

    // Get the mlid.
    $mlid = 0;
    if (!empty($form_state['node']->menu['mlid'])) {
      $mlid = $form_state['node']->menu['mlid'];
    }

    if (!empty($menuname)) {
      $wrapper = array(
        '#type'   => 'markup',
        '#theme'  => 'better_node_menu_menu',
      );
      $form['bettermenu']['menu_elements'] = array_merge($wrapper, better_node_menu_get_menu_form($menuname, $mlid, $langcode));
    }
    $form['bettermenu']['mlid'] = array(
      '#type'   => 'hidden',
      '#value'  => $mlid,
    );
    $form['bettermenu']['#theme'] = 'better_node_menu_form';

    // Put the extra submit and validate functions at the beginning.
    array_unshift($form['#submit'], 'better_node_menu_save_form');
    array_unshift($form['#validate'], 'better_node_menu_validate_form');
  }
}

/**
 * Getter for the node form language.
 *
 * @param array $form
 *   The form variable.
 * @param array $form_state
 *   The form state variable.
 *
 * @return string
 *   The language code.
 */
function better_node_menu_get_node_form_language(array $form, array $form_state) {
  // Undefined by default.
  $langcode = LANGUAGE_NONE;

  // If it's already set as a value in the form state variable.
  if (isset($form_state['values']['language'])) {
    $langcode = $form_state['values']['language'];
  }
  // Otherwise, if the node already has a language.
  elseif (isset($form['#node']->language) && $form['#node']->language != LANGUAGE_NONE) {
    $langcode = $form['#node']->language;
  }
  // Or it's the first language in the available languages from i18n.
  elseif (module_exists('i18n_menu') && i18n_node_type_enabled($form['#node'])) {
    $language_options = array_keys(i18n_node_language_list($form['#node'], TRUE, TRUE));
    if (!empty($language_options)) {
      $langcode = array_shift($language_options);
    }
  }

  return $langcode;
}

/**
 * Get the menu form access.
 *
 * @return bool
 *   Does the user have access to edit menus.
 */
function better_node_menu_form_access() {
  // Determine the general access.
  if (module_exists('menu_admin_per_menu') && function_exists('_menu_admin_per_menu_access')) {
    return _menu_admin_per_menu_access();
  }
  else {
    return user_access('administer menu');
  }
}

/**
 * Get all available menus we can add nodes to.
 *
 * @return array
 *   All available menus.
 */
function better_node_menu_get_menus($type = FALSE, $langcode = NULL) {
  // First get the menus.
  // Code is based on menu_get_menus().
  $all_menus = array();
  if ($all_menus = menu_load_all()) {
    foreach ($all_menus as $menu_name => $menu) {
      $all_menus[$menu_name] = t($menu['title'], array(), array('langcode' => $langcode));
    }
    asort($all_menus);
  }

  if ($type === FALSE) {
    return $all_menus;
  }
  $menus = variable_get('menu_options_' . $type, array('main-menu' => 'main-menu'));
  $to_return = array();
  foreach ($menus as $menu) {
    if (isset($all_menus[$menu])) {
      $all_menus[$menu] = t($all_menus[$menu], array(), array('langcode' => $langcode));
      $to_return[$menu] = $all_menus[$menu];
    }
  }

  foreach ($to_return as $menuname => $title) {
    $menu = menu_load($menuname);

    // Filter access.
    if (module_exists('menu_admin_per_menu') && function_exists('_menu_admin_per_menu_menu_access')) {
      if (!_menu_admin_per_menu_menu_access($menu)) {
        unset($to_return[$menuname]);
      }
    }

    // Filter by language.
    if (module_exists('i18n_menu')) {
      if (!in_array($menu['language'], array($langcode, LANGUAGE_NONE))) {
        unset($to_return[$menuname]);
      }
    }
  }

  return $to_return;
}

/**
 * Get the menu form.
 *
 * @param string $menu_name
 *   The menu we want to retrieve.
 * @param int $active_mlid
 *   The id of the active menu link.
 *
 * @return array
 *   A Drupal render array
 */
function better_node_menu_get_menu_form($menu_name, $active_mlid = FALSE, $langcode = NULL) {
  $tree = menu_tree_all_data($menu_name);

  if (module_exists('i18n_menu')) {
    if (i18n_menu_mode($menu_name)) {
      // Prepare the tree before localizing it.
      better_node_menu_prepare_menu($tree, $langcode);
    }

    if (function_exists('i18n_menu_localize_tree')) {
      // Localize the tree.
      $tree = i18n_menu_localize_tree($tree, $langcode);
    }
  }

  // Prepare the menu for rendering using a recursive helper.
  $menu = array();
  $has_active = better_node_menu_create_form_table($menu, $tree, $active_mlid);

  // Add a new link field when there's no existing menu item.
  if (!$active_mlid) {
    $menu = array_merge(
      array(
        'mlid:newlink' => array(
          '#item' => array(
            'menu_name'       => $menu_name,
            'mlid'            => 'newlink',
            'plid'            => 0,
            'weight'          => -50,
            'has_children'    => 0,
            'link_title'      => '',
            'depth'           => 0,
            'hidden'          => TRUE,
          ),
          '#attributes' => array(
            'class'           => array('active'),
          ),
          'link_title' => array(
            '#type'           => 'textfield',
            '#maxlength'      => 255,
            '#default_value'  => t('Please enter a name for this menu item.'),
            '#required' => TRUE,
          ),
          'weight' => array(
            '#type'           => 'weight',
            '#delta'          => 50,
            '#default_value'  => -50,
            '#title_display'  => 'invisible',
            '#title'          => t('Weight for @title', array('@title' => '')),
          ),
          'mlid' => array(
            '#type'           => 'hidden',
            '#value'          => 'newlink',
          ),
          'plid' => array(
            '#type'           => 'hidden',
            '#default_value'  => 0,
          ),
        ),
      ),
    $menu);
  }
  elseif (!$has_active) {
    $item = menu_link_load($active_mlid);
    $item['plid'] = 0;
    $item['depth'] = 0;
    $item['weight'] = -50;
    $item['menu_name'] = $menu_name;
    for ($i = 1; $i < 10; $i++) {
      $item['p' . $i] = 0;
    }

    $menu = array_merge(
      array(
        'mlid:' . $active_mlid => array(
          '#item' => $item,
          'link_title' => array(
            '#type' => 'textfield',
            '#maxlength' => 255,
            '#default_value' => $item['title'],
            '#required' => TRUE,
          ),
          'weight' => array(
            '#type' => 'weight',
            '#delta' => 50,
            '#default_value' => $item['weight'],
            '#title_display' => 'invisible',
            '#title' => t('Weight for @title', array('@title' => $item['title'])),
          ),
          'mlid' => array(
            '#type' => 'hidden',
            '#value' => $item['mlid'],
          ),
          'plid' => array(
            '#type' => 'hidden',
            '#default_value' => $item['plid'],
          ),
          '#attributes' => array(
            'class' => array('active'),
          ),
        ),
      ),
    $menu);
  }

  return $menu;
}

/**
 * Recursively prepare the menu for localization.
 *
 * @param array $tree
 *   The menu tree.
 */
function better_node_menu_prepare_menu(&$tree = array(), $langcode = LANGUAGE_NONE) {
  foreach ($tree as $key => $item) {
    // Remove the menu item if it's not in the current language.
    // Regardless of other parameters.
    if ($langcode != LANGUAGE_NONE && isset($item['link']['language']) && $item['link']['language'] != $langcode) {
      unset($tree[$key]);
    }
    elseif (!empty($item['below'])) {
      better_node_menu_prepare_menu($item['below'], $langcode);
    }
  }
}

/**
 * Create the menu form elements.
 *
 * @param array $menu
 *   The menu that has to be created.
 * @param array $tree
 *   The menu as returned from menu_tree_data.
 * @param int $active_mlid
 *   The id of the active menu item.
 * @param bool $has_active
 *   A trigger to identify whether the current menu contains the active item.
 *
 * @return bool
 *   A trigger to identify whether the current menu contains the active item.
 */
function better_node_menu_create_form_table(array &$menu, array $tree, $active_mlid = FALSE, $has_active = FALSE) {
  // See _menu_overview_tree_form() in menu.admin.inc.
  foreach ($tree as $data) {
    $item = $data['link'];
    // Don't show callbacks; these have $item['hidden'] < 0.
    if ($item && $item['hidden'] >= 0) {
      $mlid = 'mlid:' . $item['mlid'];
      $menu[$mlid]['#item'] = $item;

      // Provide an input field for the active menu item.
      if ($item['mlid'] == $active_mlid) {
        $menu[$mlid]['link_title'] = array(
          '#type' => 'textfield',
          '#maxlength' => 255,
          '#default_value' => $item['title'],
          '#required' => TRUE,
        );
        $has_active = TRUE;
      }
      else {
        $menu[$mlid]['link_title']['#markup'] = $item['title'];
        if ($item['hidden'] && !module_exists('i18n_menu')) {
          $menu[$mlid]['link_title']['#markup'] .= ' ' . t('(disabled)');
        }
        elseif ($item['link_path'] == 'user' && $item['module'] == 'system') {
          $menu[$mlid]['link_title']['#markup'] .= ' ' . t('(logged in users only)');
        }
      }

      $menu[$mlid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => $item['weight'],
        '#title_display' => 'invisible',
        '#title' => t('Weight for @title', array('@title' => $item['title'])),
      );
      $menu[$mlid]['mlid'] = array(
        '#type' => 'hidden',
        '#value' => $item['mlid'],
      );
      $menu[$mlid]['plid'] = array(
        '#type' => 'hidden',
        '#default_value' => $item['plid'],
      );
      if ($item['mlid'] == $active_mlid) {
        $menu[$mlid]['#attributes'] = array('class' => array('active'));
      }
    }

    if ($data['below']) {
      $has_active = better_node_menu_create_form_table($menu, $data['below'], $active_mlid, $has_active);
    }
  }

  return $has_active;
}

/**
 * Theme the menu.
 *
 * @param array $variables
 *   An array of variables necessary to theme the menu.
 *
 * @return string
 *   The rendered menu in HTML.
 */
function theme_better_node_menu_menu(array $variables) {
  drupal_add_tabledrag('menu-overview', 'match', 'parent', 'menu-plid', 'menu-plid', 'menu-mlid', TRUE, MENU_MAX_DEPTH - 1);
  drupal_add_tabledrag('menu-overview', 'order', 'sibling', 'menu-weight');

  $output = '';

  $rows = array();
  if (!empty($variables['form'])) {
    $menu = $variables['form'];
    foreach (element_children($menu) as $mlid) {
      $item = $menu[$mlid];
      if (isset($item['#item']['hidden'])) {
        $element = $item;

        // Add special classes to be used for tabledrag.js.
        $element['plid']['#attributes']['class'] = array('menu-plid');
        $element['mlid']['#attributes']['class'] = array('menu-mlid');
        $element['weight']['#attributes']['class'] = array('menu-weight');

        // Change the parent field to a hidden. This allows any value but hides
        // the field.
        $element['plid']['#type'] = 'hidden';

        $row = array();
        $row[] = theme('indentation', array('size' => $element['#item']['depth'] - 1)) . drupal_render($element['link_title']);
        $row[] = drupal_render($element['weight']) . drupal_render($element['plid']) . drupal_render($element['mlid']);
        $row = array_merge(array('data' => $row), $element['#attributes']);
        $row['class'][] = 'draggable';
        if (!empty($element['active'])) {
          $row['class'][] = 'active';
        }

        $rows[] = $row;
      }
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No menu links have been found'), 'colspan' => '7'));
  }
  $output .= theme('table', array('rows' => $rows, 'attributes' => array('id' => 'menu-overview')));

  return $output;
}

/**
 * Theme the form.
 *
 * @param array $variables
 *   An array of variables necessary to theme the form.
 *
 * @return string
 *   The HTML of the themed form.
 */
function theme_better_node_menu_form(array $variables) {
  $path_assets = drupal_get_path('module', 'better_node_menu') . '/assets';

  drupal_add_js($path_assets . '/js/better_node_menu.js');
  drupal_add_css($path_assets . '/css/better_node_menu.css');

  $form = $variables['form'];

  $output = '<div id="bettermenu-wrapper">';
  $output .= drupal_render($form['menu_name']);
  $output .= '<div id="bettermenu-position">';

  // Only theme the table if there are menu items available.
  if (isset($form['menu_elements'])) {
    $children = element_children($form['menu_elements']);
    if (!empty($children)) {
      $output .= theme('better_node_menu_menu', array('form' => $form['menu_elements']));
    }
    else {
      $output .= drupal_render($form['menu_elements']);
    }
  }

  $output .= '</div></div>';

  return $output;
}

/**
 * AHAH Callback after changing the menu.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return string
 *   HTML of the themed menu.
 */
function better_node_menu_menuname_callback(array $form, array $form_state) {
  // Manually reset the plid on the current menu link, since
  // form_state['rebuild'] = TRUE does not work in order to clear the value.
  if (!empty($form_state['values']['bettermenu']['mlid']) &&
      !empty($form['bettermenu']['menu_elements'])) {
    $mlid = $form_state['values']['bettermenu']['mlid'];
    $default_plid = $form['bettermenu']['menu_elements']['mlid:' . $mlid]['plid']['#default_value'];
    $children = element_children($form['bettermenu']['menu_elements']);
    $parent_exists = FALSE;

    foreach ($children as $key) {
      if ($key == 'mlid:' . $default_plid) {
        $parent_exists = TRUE;
        break;
      }
    }

    if ($parent_exists) {
      $form_state['values']['bettermenu']['menu_elements']['mlid:' . $mlid]['plid'] = $default_plid;
      $form['bettermenu']['menu_elements']['mlid:' . $mlid]['plid']['#value'] = $default_plid;
    }
    else {
      $form_state['values']['bettermenu']['menu_elements']['mlid:' . $mlid]['plid'] = 0;
      unset($form['bettermenu']['menu_elements']['mlid:' . $mlid]['plid']['#value']);
    }
  }

  // Theme the form.
  $output = '';
  $output .= '<div id="bettermenu-position">';
  if (!empty($form_state['values']['bettermenu']['menu_name'])) {
    $output .= theme('better_node_menu_menu', array('form' => $form['bettermenu']['menu_elements']));
  }
  else {
    $output .= drupal_render($form['bettermenu']['menu_elements']);
  }
  $output .= '</div>';

  return $output;
}

/**
 * AHAH Callback after changing the node language.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return string
 *   HTML of the themed menu form.
 */
function better_node_menu_language_callback(array $form, array $form_state) {
  // Rebuild the form upon AHAH.
  $form_state['rebuild'] = TRUE;

  // Reset the menu name if it's not present in the list.
  if (!empty($form['bettermenu']['menu_name']['#options']) &&
      !in_array($form_state['values']['bettermenu']['menu_name'], array_keys($form['bettermenu']['menu_name']['#options']), TRUE)) {
    $form_state['values']['bettermenu']['menu_name'] = 0;
  }

  $output = '<div id="bettermenu-wrapper">';
  $output .= drupal_render($form['bettermenu']['menu_name']);
  $output .= '<div id="bettermenu-position">';

  if (!empty($form['bettermenu']['menu_elements'])) {
    // Only theme the table if there are menu items available.
    $children = element_children($form['bettermenu']['menu_elements']);
    if (!empty($children)) {
      $output .= theme('better_node_menu_menu', array('form' => $form['bettermenu']['menu_elements']));
    }
  }

  $output .= '</div></div>';

  return $output;
}

/**
 * Custom form submit handler.
 */
function better_node_menu_save_form($form, &$form_state) {
  // Unset the node menu values, so that we can bypass the default core menu
  // saving.
  unset($form['#node']->menu);
  unset($form_state['node']->menu);

  // See menu_overview_form_submit() in menu.admin.inc.
  // When dealing with saving menu items, the order in which these items are
  // saved is critical. If a changed child item is saved before its parent,
  // the child item could be saved with an invalid path past its immediate
  // parent. To prevent this, save items in the form in the same order they
  // are sent by $_POST, ensuring parents are saved first, then their children.
  // See http://drupal.org/node/181126#comment-632270
  $updated_items = array();

  if (!empty($form_state['values']['bettermenu']['menu_elements']) && !empty($form['bettermenu']['menu_elements'])) {
    // Get the $_POST order.
    $order = array_flip(array_keys($form_state['values']['bettermenu']['menu_elements']));
    // Update our original form with the new order.
    $form = array_merge($order, $form['bettermenu']['menu_elements']);
    $fields = array('weight', 'plid', 'link_title');

    foreach (element_children($form) as $mlid) {
      if (isset($form[$mlid]['#item'])) {
        $element = $form[$mlid];
        // Update any fields that have changed in this menu item.
        foreach ($fields as $field) {
          if (isset($element[$field]['#value']) &&
              isset($element[$field]['#default_value']) &&
              $element[$field]['#value'] != $element[$field]['#default_value']) {
            $element['#item'][$field] = $element[$field]['#value'];
            $updated_items[$mlid] = $element['#item'];
          }
        }
        if (isset($element['hidden'])) {
          // Hidden is a special case, the value needs to be reversed.
          if ($element['hidden']['#value'] != $element['hidden']['#default_value']) {
            // Convert to integer rather than bool due to PDO cast to string.
            $element['#item']['hidden'] = $element['hidden']['#value'] ? 0 : 1;
            $updated_items[$mlid] = $element['#item'];
          }
        }
        else {
          $element['#item']['hidden'] = 0;
          $updated_items[$mlid] = $element['#item'];
        }
      }
    }
  }

  // Pass the updated items to the node object
  // This way we can use the node id in our menu link upon node save
  // and consequently fill in the dummy 'newlink' mlid with the newly saved
  // menu link.
  $form_state['node']->updated_items = $updated_items;
}

/**
 * Handle the validation of the menu form.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the submitted state of the form.
 */
function better_node_menu_validate_form(array $form, array &$form_state) {
  $bettermenu = $form_state['values']['bettermenu'];

  // The menu item needs a title.
  if (!empty($bettermenu['menu_elements'])) {
    foreach ($bettermenu['menu_elements'] as $key => $item) {
      if (isset($item['link_title']) && (!strlen($item['link_title']) || $item['link_title'] == t('Please enter a name for this menu item.'))) {
        form_set_error($key, t('Please enter a name for the menu item below.'));
      }
    }
  }
}
