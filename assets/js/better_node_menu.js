/**
 * @file
 * Adds javascript functionality to the better node menu form.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.BetterNodeMenu = {
    attach: function (context) {
      // Empty the menu item title field upon focus.
      $('#menu-overview .active input').click(function (e) {
        if ($(this).val() === Drupal.t('Please enter a name for this menu item.')) {
          $(this).val('');
        }
      }).blur(function (e) {
        if ($(this).val() === '') {
          $(this).val(Drupal.t('Please enter a name for this menu item.'));
        }
      });

      // Update the vertical tabs.
      $('#edit-bettermenu', context).drupalSetSummary(function (context) {
        var selected_menu = $('#edit-bettermenu-menu-name', context).val();
        var title = $('#menu-overview .active input', context).val();

        if (selected_menu !== '0') {
          if (typeof title !== 'undefined' && title.length) {
            return Drupal.checkPlain(title);
          }
          else {
            return Drupal.t('Please enter a name for this menu item.');
          }
        }
        else {
          return Drupal.t('Not in menu');
        }
      });
    }
  };
})(jQuery);
