INTRODUCTION
------------

Better Node Menu improves the usability when adding new menu items. It allows
users to drag menu items within the existing menu using the default Drupal
tabledrag functionality. The module is also language aware and will
automatically update the menu if the language is changed when editing or
creating a node.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

You don't need to configure anything. After enabling the module the node
add/edit form will be altered.


MAINTAINERS
-----------

Current maintainers:
 * El Phaenax (elphaenax) - https://www.drupal.org/u/elphaenax

The project has been created at and sponsored by:
 * Open Up Media
   Open Up Media is a full service brand agency, based in Antwerp, Belgium. The
   agency originated from the merger between Savant Media and Apluz. Both
   agencies brought their years of experience and expertise together to offer
   customers a wider range of services.

   We have been developing websites almost exclusively using Drupal since 2007.
   We have a complimentary team that delivers beautiful, user-friendly, and
   rock-solid websites. Visit https://www.openupmedia.be for more information.
